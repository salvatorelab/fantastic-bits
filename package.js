var fs = require('fs');

var config = {
  files: [
    'Utils.js',
    'Field.js',
    'AI.js',
    'main.js'
  ],
  output: 'output.js'
};

var output = '';
config.files.forEach(file => {
  output += removeImports(fs.readFileSync(file, 'utf8'));
});

fs.writeFile(config.output, output, (err) => {
    if (err) throw err;

    console.log('Done');
});

function removeImports(fileContent) {
  processedContent = fileContent;
  processedContent = processedContent.replace(/.*?require\(.*?\).*?$/gim, '');
  processedContent = processedContent.replace(/^.*?module\.exports.*?$/gim, '');
  return processedContent;
}
