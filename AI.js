var Utils = require('./Utils.js');

var Move = function(params) {
  this.command = function() {
    var command = 'MOVE 8000 3750 150';

    if (params.type == 'SCORE') {
      command = 'THROW' + ' ' + params.target.x + ' ' + params.target.y + ' ' + params.power;
    } else if (params.type == 'ACCIO') {
      command = 'ACCIO' + ' ' + params.target.id;
    } else if (params.type == 'PETRIFICUS') {
      command = 'PETRIFICUS' + ' ' + params.target.id;
    } else {
      command = 'MOVE' + ' ' + params.target.x + ' ' + params.target.y + ' ' + params.power;
    }

    return command;
  };

  this.isCatch = function() {
    return params.type == 'CATCH';
  };

  this.isCover = function() {
    return params.type == 'COVER';
  };

  this.isAccio = function() {
    return params.type == 'ACCIO';
  };

  this.isPetrificus = function() {
    return params.type == 'PETRIFICUS';
  };

  this.target = function() {
    return params.target;
  };

  this.targetIdIs = function(id) {
    if (!params.target) return false;
    return params.target.id == id;
  }
};

var AI = function(field) {
  var MAX_MOVE_POWER = 150;
  var MAX_THROW_POWER = 500;

  var magic = 0;

  this.setMagic = function(availableMagic) {
    magic = availableMagic;
  }

  this.moves = function() {
    var moves = [];
    var inminentEnemyScoreSnaffle = getInminentEnemyScoreSnaffle();

    var wizards = field.getWizards();
    wizards.forEach(wizard => {
      if (wizard.hasSnaffle) {
        //throw to goal
        //use spell to score
        //throw to an empty spot
        moves.push(new Move({
          type: 'SCORE',
          target: field.getEnemyGoal().center,
          power: MAX_THROW_POWER
        }));
      } else if (inminentEnemyScore(moves) && magic>=10) {
        moves.push(new Move({
          type: 'PETRIFICUS',
          target: inminentEnemyScoreSnaffle.snaffle
        }));
      } else {
        var snaffles = field.getSnaffles();
        var snaffle = findBestAvailableSnaffle(snaffles, moves, wizard);

        if (!snaffle) {
          moves.push(new Move({
            type: 'COVER',
            target: field.getOwnGoal().center,
            power: MAX_MOVE_POWER
          }));
        } else {
          if (magic >= 15 && enemyHasAdvantageOverSnaffle(wizard, snaffle)) {
            moves.push(new Move({
              type: 'ACCIO',
              target: snaffle
            }));
          } else {
            moves.push(new Move({
              type: 'CATCH',
              target: snaffle,
              power: MAX_MOVE_POWER
            }));
          }
        }
      }
    });

    return moves;
  };

  function getInminentEnemyScoreSnaffle() {
    var SNAFFLE_TOO_CLOSE = 2000;

    var snaffles = field.getSnaffles();
    var goal = field.getOwnGoal();

    var closestSnaffleToOurGoal = null;
    snaffles.forEach(snaffle => {
      var d = Utils.distanceBetween(snaffle, goal.center);
      if (snaffle.vx != 0 && snaffle.vy != 0 &&
        d<SNAFFLE_TOO_CLOSE &&
        (!closestSnaffleToOurGoal || closestSnaffleToOurGoal.distance > d)) {
        closestSnaffleToOurGoal = {
          snaffle: snaffle,
          distance: d
        };
      }
    });

    return closestSnaffleToOurGoal;
  }

  function inminentEnemyScore(moves) {
    var WIZARD_TOO_FAR_FROM_GOAL = 500;

    var goal = field.getOwnGoal();
    var wizards = field.getWizards();

    var closestSnaffleToOurGoal = getInminentEnemyScoreSnaffle();

    var wizardsTooFar = true;
    wizards.forEach(wizard => {
      var d = Utils.distanceBetween(wizard, goal.center);
      if (d<WIZARD_TOO_FAR_FROM_GOAL) wizardsTooFar = false;
    });

    var duplicatedMove = false;
    moves.forEach(move => {
      if (move.isPetrificus() && move.targetIdIs(closestSnaffleToOurGoal.snaffle.id)) duplicatedMove = true;
    });

    return (closestSnaffleToOurGoal && wizardsTooFar && !duplicatedMove);
  }

  function wizardIsCloserThanSnaffleToEnemyGoal(wizard, snaffle) {
    var enemyGoal = field.getEnemyGoal();
    var wizardDistance = Utils.distanceBetween(wizard, {x: enemyGoal.center.x, y: enemyGoal.center.y});
    var snaffleDistance = Utils.distanceBetween(snaffle, {x: enemyGoal.center.x, y: enemyGoal.center.y});
    return wizardDistance < snaffleDistance;
  }

  function enemyHasAdvantageOverSnaffle(wizard, snaffle) {
    var distance = 0;
    snaffle.distances.wizards.forEach(wizardDistance => {
      if (wizardDistance.wizard.id == wizard.id) distance = wizardDistance.distance;
    });

    var enemyHasAdvantageOverSnaffle = false;
    snaffle.distances.enemies.forEach(enemyDistance => {
      if (enemyDistance.distance < distance && wizardIsCloserThanSnaffleToEnemyGoal(wizard, snaffle)) {
        enemyHasAdvantageOverSnaffle = true;
      }
    });

    return enemyHasAdvantageOverSnaffle;
  }

  function findBestAvailableSnaffle(snaffles, moves, wizard) {
    if (snaffles.length == 0) return null;

    var goal = field.getOwnGoal();
    var availableTargets = [];
    snaffles.forEach(snaffle => {
      if (!wizardMovingToCatchSnaffle(moves, snaffle)) {
        var distance = Utils.distanceFromWizardToSnaffle(wizard, snaffle);
        var distanceToOurGoal = Utils.distanceBetween(snaffle, {x: goal.center.x, y: goal.center.y});
        availableTargets.push({
          target: snaffle,
          score: - distance - distanceToOurGoal
        });
      }
    });

    if (availableTargets.length == 0) return null;

    var best = null;
    availableTargets.forEach(target => {
      if (!best || best.score < target.score) {
        best = target;
      }
    });
    return best.target;
  }

  function wizardMovingToCatchSnaffle(moves, snaffle) {
    if (moves.length == 0) return false;

    var wizardMovingToCatchSnaffle = false;
    moves.forEach(move => {
      if (move.isCatch() && move.target().id === snaffle.id) {
        wizardMovingToCatchSnaffle = true;
      }
    });
    return wizardMovingToCatchSnaffle;
  }
};

module.exports = AI;
