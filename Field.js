var Utils = require('./Utils.js');

/**
* The coordinates X=0, Y=0 represent the pixel at the top-left.
*/
var Field = function(myTeamId) {
  var POLE_RADIUS = 300;
  var SNAFFLE_RADIUS = 150;
  var WIZARD_RADIUS = 400;

  var width = 16001;
  var height = 7501;

  var goals = [
    {
      poles: [{x: 0, y: 1750}, {x: 0, y: 5750}],
      center: {x: 0, y: 3750}
    },
    {
      poles: [{x: 16000, y: 1750}, {x: 16000, y: 5750}],
      center: {x: 16000, y: 3750}
    }
  ];

  var snaffles = [];
  var wizards = [];
  var enemyWizards = [];

  this.getEnemyGoal = function() {
    if (myTeamId === 0) return goals[1];

    return goals[0];
  };

  this.getOwnGoal = function() {
    if (myTeamId === 1) return goals[1];

    return goals[0];
  };

  this.addSnaffle = function(params) {
    snaffles.push({
      id: params.id,
      x: params.x,
      y: params.y,
      vx: params.vx,
      vy: params.vy,
      owned: params.state === 1
    });
  };

  this.getSnaffles = function() {
    return snaffles;
  };

  this.addWizard = function(params) {
    wizards.push({
      id: params.id,
      x: params.x,
      y: params.y,
      vx: params.vx,
      vy: params.vy,
      hasSnaffle: params.state === 1
    });
  };

  this.getWizards = function() {
    return wizards;
  };

  this.addEnemyWizard = function(params) {
    enemyWizards.push({
      id: params.id,
      x: params.x,
      y: params.y,
      vx: params.vx,
      vy: params.vy,
      hasSnaffle: params.state === 1
    });
  };

  this.getEnemyWizards = function() {
    return enemyWizards;
  };

  this.calculateDistances = function() {
    snaffles.forEach(snaffle => {
      snaffle.distances = {
        enemies: Utils.distancesBetween(snaffle, enemyWizards),
        wizards: Utils.distancesBetween(snaffle, wizards)
      };
    });
  };

  this.getDescription = function() {
    var description = '';

    snaffles.forEach(snaffle => {
      var owned = snaffle.owned ? 'owned ' : '';
      description += owned + 'snaffle #' + snaffle.id + ' on (' + snaffle.x + ', ' + snaffle.y + ') going at velocity (' + snaffle.vx + ', ' + snaffle.vy + ')';
      description += '\n'
    });

    wizards.forEach(wizard => {
      var hasSnaffle = wizard.hasSnaffle ? ' has a snaffle' : '';
      description += 'wizard #' + wizard.id + ' on (' + wizard.x + ', ' + wizard.y + ') going at velocity (' + wizard.vx + ', ' + wizard.vy + ')' + hasSnaffle;
      description += '\n'
    });

    enemyWizards.forEach(wizard => {
      var hasSnaffle = wizard.hasSnaffle ? ' has a snaffle' : '';
      description += 'enemy wizard #' + wizard.id + ' on (' + wizard.x + ', ' + wizard.y + ') going at velocity (' + wizard.vx + ', ' + wizard.vy + ')' + hasSnaffle;
      description += '\n'
    });

    return description;
  };
};

module.exports = Field;
