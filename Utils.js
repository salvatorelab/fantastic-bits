var Utils = {
  distanceFromWizardToSnaffle: function(wizard, snaffle) {
    return Utils.distanceBetween(wizard, snaffle);
  },

  distancesBetween: function(snaffle, wizardList) {
    var distances = [];
    wizardList.forEach(wizard => {
      distances.push({
        wizard: wizard,
        distance: Utils.distanceFromWizardToSnaffle(wizard, snaffle)
      });
    });
    return distances;
  },

  distanceBetween: function(a, b) {
    var x = a.x - b.x;
    var y = a.y - b.y;
    return Math.sqrt(x*x + y*y);
  }
};

module.exports = Utils;
