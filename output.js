var Utils = {
  distanceFromWizardToSnaffle: function(wizard, snaffle) {
    return Utils.distanceBetween(wizard, snaffle);
  },

  distancesBetween: function(snaffle, wizardList) {
    var distances = [];
    wizardList.forEach(wizard => {
      distances.push({
        wizard: wizard,
        distance: Utils.distanceFromWizardToSnaffle(wizard, snaffle)
      });
    });
    return distances;
  },

  distanceBetween: function(a, b) {
    var x = a.x - b.x;
    var y = a.y - b.y;
    return Math.sqrt(x*x + y*y);
  }
};




/**
* The coordinates X=0, Y=0 represent the pixel at the top-left.
*/
var Field = function(myTeamId) {
  var POLE_RADIUS = 300;
  var SNAFFLE_RADIUS = 150;
  var WIZARD_RADIUS = 400;

  var width = 16001;
  var height = 7501;

  var goals = [
    {
      poles: [{x: 0, y: 1750}, {x: 0, y: 5750}],
      center: {x: 0, y: 3750}
    },
    {
      poles: [{x: 16000, y: 1750}, {x: 16000, y: 5750}],
      center: {x: 16000, y: 3750}
    }
  ];

  var snaffles = [];
  var wizards = [];
  var enemyWizards = [];

  this.getEnemyGoal = function() {
    if (myTeamId === 0) return goals[1];

    return goals[0];
  };

  this.getOwnGoal = function() {
    if (myTeamId === 1) return goals[1];

    return goals[0];
  };

  this.addSnaffle = function(params) {
    snaffles.push({
      id: params.id,
      x: params.x,
      y: params.y,
      vx: params.vx,
      vy: params.vy,
      owned: params.state === 1
    });
  };

  this.getSnaffles = function() {
    return snaffles;
  };

  this.addWizard = function(params) {
    wizards.push({
      id: params.id,
      x: params.x,
      y: params.y,
      vx: params.vx,
      vy: params.vy,
      hasSnaffle: params.state === 1
    });
  };

  this.getWizards = function() {
    return wizards;
  };

  this.addEnemyWizard = function(params) {
    enemyWizards.push({
      id: params.id,
      x: params.x,
      y: params.y,
      vx: params.vx,
      vy: params.vy,
      hasSnaffle: params.state === 1
    });
  };

  this.getEnemyWizards = function() {
    return enemyWizards;
  };

  this.calculateDistances = function() {
    snaffles.forEach(snaffle => {
      snaffle.distances = {
        enemies: Utils.distancesBetween(snaffle, enemyWizards),
        wizards: Utils.distancesBetween(snaffle, wizards)
      };
    });
  };

  this.getDescription = function() {
    var description = '';

    snaffles.forEach(snaffle => {
      var owned = snaffle.owned ? 'owned ' : '';
      description += owned + 'snaffle #' + snaffle.id + ' on (' + snaffle.x + ', ' + snaffle.y + ') going at velocity (' + snaffle.vx + ', ' + snaffle.vy + ')';
      description += '\n'
    });

    wizards.forEach(wizard => {
      var hasSnaffle = wizard.hasSnaffle ? ' has a snaffle' : '';
      description += 'wizard #' + wizard.id + ' on (' + wizard.x + ', ' + wizard.y + ') going at velocity (' + wizard.vx + ', ' + wizard.vy + ')' + hasSnaffle;
      description += '\n'
    });

    enemyWizards.forEach(wizard => {
      var hasSnaffle = wizard.hasSnaffle ? ' has a snaffle' : '';
      description += 'enemy wizard #' + wizard.id + ' on (' + wizard.x + ', ' + wizard.y + ') going at velocity (' + wizard.vx + ', ' + wizard.vy + ')' + hasSnaffle;
      description += '\n'
    });

    return description;
  };
};




var Move = function(params) {
  this.command = function() {
    var command = 'MOVE 8000 3750 150';

    if (params.type == 'SCORE') {
      command = 'THROW' + ' ' + params.target.x + ' ' + params.target.y + ' ' + params.power;
    } else if (params.type == 'ACCIO') {
      command = 'ACCIO' + ' ' + params.target.id;
    } else if (params.type == 'PETRIFICUS') {
      command = 'PETRIFICUS' + ' ' + params.target.id;
    } else {
      command = 'MOVE' + ' ' + params.target.x + ' ' + params.target.y + ' ' + params.power;
    }

    return command;
  };

  this.isCatch = function() {
    return params.type == 'CATCH';
  };

  this.isCover = function() {
    return params.type == 'COVER';
  };

  this.isAccio = function() {
    return params.type == 'ACCIO';
  };

  this.isPetrificus = function() {
    return params.type == 'PETRIFICUS';
  };

  this.target = function() {
    return params.target;
  };

  this.targetIdIs = function(id) {
    if (!params.target) return false;
    return params.target.id == id;
  }
};

var AI = function(field) {
  var MAX_MOVE_POWER = 150;
  var MAX_THROW_POWER = 500;

  var magic = 0;

  this.setMagic = function(availableMagic) {
    magic = availableMagic;
  }

  this.moves = function() {
    var moves = [];
    var inminentEnemyScoreSnaffle = getInminentEnemyScoreSnaffle();

    var wizards = field.getWizards();
    wizards.forEach(wizard => {
      if (wizard.hasSnaffle) {
        //throw to goal
        //use spell to score
        //throw to an empty spot
        moves.push(new Move({
          type: 'SCORE',
          target: field.getEnemyGoal().center,
          power: MAX_THROW_POWER
        }));
      } else if (inminentEnemyScore(moves) && magic>=10) {
        moves.push(new Move({
          type: 'PETRIFICUS',
          target: inminentEnemyScoreSnaffle.snaffle
        }));
      } else {
        var snaffles = field.getSnaffles();
        var snaffle = findBestAvailableSnaffle(snaffles, moves, wizard);

        if (!snaffle) {
          moves.push(new Move({
            type: 'COVER',
            target: field.getOwnGoal().center,
            power: MAX_MOVE_POWER
          }));
        } else {
          if (magic >= 15 && enemyHasAdvantageOverSnaffle(wizard, snaffle)) {
            moves.push(new Move({
              type: 'ACCIO',
              target: snaffle
            }));
          } else {
            moves.push(new Move({
              type: 'CATCH',
              target: snaffle,
              power: MAX_MOVE_POWER
            }));
          }
        }
      }
    });

    return moves;
  };

  function getInminentEnemyScoreSnaffle() {
    var SNAFFLE_TOO_CLOSE = 2000;

    var snaffles = field.getSnaffles();
    var goal = field.getOwnGoal();

    var closestSnaffleToOurGoal = null;
    snaffles.forEach(snaffle => {
      var d = Utils.distanceBetween(snaffle, goal.center);
      if (snaffle.vx != 0 && snaffle.vy != 0 &&
        d<SNAFFLE_TOO_CLOSE &&
        (!closestSnaffleToOurGoal || closestSnaffleToOurGoal.distance > d)) {
        closestSnaffleToOurGoal = {
          snaffle: snaffle,
          distance: d
        };
      }
    });

    return closestSnaffleToOurGoal;
  }

  function inminentEnemyScore(moves) {
    var WIZARD_TOO_FAR_FROM_GOAL = 500;

    var goal = field.getOwnGoal();
    var wizards = field.getWizards();

    var closestSnaffleToOurGoal = getInminentEnemyScoreSnaffle();

    var wizardsTooFar = true;
    wizards.forEach(wizard => {
      var d = Utils.distanceBetween(wizard, goal.center);
      if (d<WIZARD_TOO_FAR_FROM_GOAL) wizardsTooFar = false;
    });

    var duplicatedMove = false;
    moves.forEach(move => {
      if (move.isPetrificus() && move.targetIdIs(closestSnaffleToOurGoal.snaffle.id)) duplicatedMove = true;
    });

    return (closestSnaffleToOurGoal && wizardsTooFar && !duplicatedMove);
  }

  function wizardIsCloserThanSnaffleToEnemyGoal(wizard, snaffle) {
    var enemyGoal = field.getEnemyGoal();
    var wizardDistance = Utils.distanceBetween(wizard, {x: enemyGoal.center.x, y: enemyGoal.center.y});
    var snaffleDistance = Utils.distanceBetween(snaffle, {x: enemyGoal.center.x, y: enemyGoal.center.y});
    return wizardDistance < snaffleDistance;
  }

  function enemyHasAdvantageOverSnaffle(wizard, snaffle) {
    var distance = 0;
    snaffle.distances.wizards.forEach(wizardDistance => {
      if (wizardDistance.wizard.id == wizard.id) distance = wizardDistance.distance;
    });

    var enemyHasAdvantageOverSnaffle = false;
    snaffle.distances.enemies.forEach(enemyDistance => {
      if (enemyDistance.distance < distance && wizardIsCloserThanSnaffleToEnemyGoal(wizard, snaffle)) {
        enemyHasAdvantageOverSnaffle = true;
      }
    });

    return enemyHasAdvantageOverSnaffle;
  }

  function findBestAvailableSnaffle(snaffles, moves, wizard) {
    if (snaffles.length == 0) return null;

    var goal = field.getOwnGoal();
    var availableTargets = [];
    snaffles.forEach(snaffle => {
      if (!wizardMovingToCatchSnaffle(moves, snaffle)) {
        var distance = Utils.distanceFromWizardToSnaffle(wizard, snaffle);
        var distanceToOurGoal = Utils.distanceBetween(snaffle, {x: goal.center.x, y: goal.center.y});
        availableTargets.push({
          target: snaffle,
          score: - distance - distanceToOurGoal
        });
      }
    });

    if (availableTargets.length == 0) return null;

    var best = null;
    availableTargets.forEach(target => {
      if (!best || best.score < target.score) {
        best = target;
      }
    });
    return best.target;
  }

  function wizardMovingToCatchSnaffle(moves, snaffle) {
    if (moves.length == 0) return false;

    var wizardMovingToCatchSnaffle = false;
    moves.forEach(move => {
      if (move.isCatch() && move.target().id === snaffle.id) {
        wizardMovingToCatchSnaffle = true;
      }
    });
    return wizardMovingToCatchSnaffle;
  }
};




/**
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 **/

var myTeamId = parseInt(readline()); // if 0 you need to score on the right of the map, if 1 you need to score on the left

// game loop
while (true) {
  var field = new Field(myTeamId);
  var ai = new AI(field);

  var inputs = readline().split(' ');
  var myScore = parseInt(inputs[0]);
  var myMagic = parseInt(inputs[1]);
  var inputs = readline().split(' ');
  var opponentScore = parseInt(inputs[0]);
  var opponentMagic = parseInt(inputs[1]);
  var entities = parseInt(readline()); // number of entities still in game
  for (var i = 0; i < entities; i++) {
    var inputs = readline().split(' ');
    var entityId = parseInt(inputs[0]); // entity identifier
    var entityType = inputs[1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
    var x = parseInt(inputs[2]); // position
    var y = parseInt(inputs[3]); // position
    var vx = parseInt(inputs[4]); // velocity
    var vy = parseInt(inputs[5]); // velocity
    var state = parseInt(inputs[6]); // 1 if the wizard is holding a Snaffle, 0 otherwise

    if (entityType == 'SNAFFLE') {
      field.addSnaffle({
        id: entityId,
        x: x,
        y: y,
        vx: vx,
        vy: vy,
        state: state
      });
    } else if (entityType == 'WIZARD') {
      field.addWizard({
        id: entityId,
        x: x,
        y: y,
        vx: vx,
        vy: vy,
        state: state
      });
    } else if (entityType == 'OPPONENT_WIZARD') {
      field.addEnemyWizard({
        id: entityId,
        x: x,
        y: y,
        vx: vx,
        vy: vy,
        state: state
      });
    }
  }

  ai.setMagic(myMagic);

  field.calculateDistances();
  var moves = ai.moves();
  moves.forEach(move => {
    print(move.command());
  });
}
