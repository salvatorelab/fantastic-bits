var Field = require('./Field.js');
var AI = require('./AI.js');
/**
 * Grab Snaffles and try to throw them through the opponent's goal!
 * Move towards a Snaffle and use your team id to determine where you need to throw it.
 **/

var myTeamId = parseInt(readline()); // if 0 you need to score on the right of the map, if 1 you need to score on the left

// game loop
while (true) {
  var field = new Field(myTeamId);
  var ai = new AI(field);

  var inputs = readline().split(' ');
  var myScore = parseInt(inputs[0]);
  var myMagic = parseInt(inputs[1]);
  var inputs = readline().split(' ');
  var opponentScore = parseInt(inputs[0]);
  var opponentMagic = parseInt(inputs[1]);
  var entities = parseInt(readline()); // number of entities still in game
  for (var i = 0; i < entities; i++) {
    var inputs = readline().split(' ');
    var entityId = parseInt(inputs[0]); // entity identifier
    var entityType = inputs[1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
    var x = parseInt(inputs[2]); // position
    var y = parseInt(inputs[3]); // position
    var vx = parseInt(inputs[4]); // velocity
    var vy = parseInt(inputs[5]); // velocity
    var state = parseInt(inputs[6]); // 1 if the wizard is holding a Snaffle, 0 otherwise

    if (entityType == 'SNAFFLE') {
      field.addSnaffle({
        id: entityId,
        x: x,
        y: y,
        vx: vx,
        vy: vy,
        state: state
      });
    } else if (entityType == 'WIZARD') {
      field.addWizard({
        id: entityId,
        x: x,
        y: y,
        vx: vx,
        vy: vy,
        state: state
      });
    } else if (entityType == 'OPPONENT_WIZARD') {
      field.addEnemyWizard({
        id: entityId,
        x: x,
        y: y,
        vx: vx,
        vy: vy,
        state: state
      });
    }
  }

  ai.setMagic(myMagic);

  field.calculateDistances();
  var moves = ai.moves();
  moves.forEach(move => {
    print(move.command());
  });
}
