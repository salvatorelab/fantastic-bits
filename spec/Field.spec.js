var Field = require('../Field.js');


describe("The Field", function() {
  it("the enemy goal is on the right side of the field when my team id=0", function() {
    var field = new Field(0);

    var goal = field.getEnemyGoal();
    expect(goal.poles[0].x).toBe(16000);
    expect(goal.poles[1].x).toBe(16000);
  });

  it("my goal is on the left side of the field when my team id=0", function() {
    var field = new Field(0);

    var goal = field.getOwnGoal();
    expect(goal.poles[0].x).toBe(0);
    expect(goal.poles[1].x).toBe(0);
  });

  it("the enemy goal is on the left side of the field when my team id=1", function() {
    var field = new Field(1);

    var goal = field.getEnemyGoal();
    expect(goal.poles[0].x).toBe(0);
    expect(goal.poles[1].x).toBe(0);
  });

  it("my goal is on the right side of the field when my team id=1", function() {
    var field = new Field(1);

    var goal = field.getOwnGoal();
    expect(goal.poles[0].x).toBe(16000);
    expect(goal.poles[1].x).toBe(16000);
  });
});

describe("Snaffles on the field", function() {
  it("a snaffle is added to the field", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owned
    });

    var snaffles = field.getSnaffles();
    expect(snaffles.length).toBe(1);
  });

  it("field description contains a line mentioning a snaffle owned by nobody", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owned
    });

    var description = field.getDescription();
    expect(description).toContain('snaffle #1 on (1000, 2000) going at velocity (10, 20)');
  });

  it("field description contains a line mentioning a snaffle owned by a player", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owned
    });

    var description = field.getDescription();
    expect(description).toContain('owned snaffle #1 on (1000, 2000) going at velocity (10, 20)');
  });
});

describe("Wizards on the field", function() {
  it("a wizard is added to the field", function() {
    var field = new Field(0);
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var wizards = field.getWizards();
    expect(wizards.length).toBe(1);
  });

  it("description contains a line mentioning a wizard that has no snaffle", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owned
    });
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var description = field.getDescription();
    expect(description).toContain('wizard #1 on (1000, 2000) going at velocity (10, 20)');
  });

  it("description contains a line mentioning a wizard that has a snaffle", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owned
    });
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owning a snaffle
    });

    var description = field.getDescription();
    expect(description).toContain('wizard #1 on (1000, 2000) going at velocity (10, 20) has a snaffle');
  });

  it("an enemy wizard is added to the field", function() {
    var field = new Field(0);
    field.addEnemyWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var wizards = field.getEnemyWizards();
    expect(wizards.length).toBe(1);
  });

  it("description contains a line mentioning an enemy wizard that has no snaffle", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owned
    });
    field.addEnemyWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var description = field.getDescription();
    expect(description).toContain('enemy wizard #1 on (1000, 2000) going at velocity (10, 20)');
  });

  it("description contains a line mentioning an enemy wizard that has a snaffle", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owned
    });
    field.addEnemyWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owning a snaffle
    });

    var description = field.getDescription();
    expect(description).toContain('enemy wizard #1 on (1000, 2000) going at velocity (10, 20) has a snaffle');
  });
});

describe("Field distances", function() {
  it("snaffle distance to enemy", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 10,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });
    field.addWizard({
      id: 1,
      x: 10,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });

    field.calculateDistances();
    var snaffles = field.getSnaffles();
    var distances = snaffles[0].distances;
    expect(distances.wizards[0].distance).toBe(0);
  });

  it("snaffle distance to enemy", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 10,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });
    field.addEnemyWizard({
      id: 1,
      x: 10,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });

    field.calculateDistances();
    var snaffles = field.getSnaffles();
    var distances = snaffles[0].distances;
    expect(distances.enemies[0].distance).toBe(0);
  });
});
