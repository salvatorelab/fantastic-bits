var Field = require('../Field.js');
var AI = require('../AI.js');


describe("The AI", function() {
  it("should order a wizard to throw a snaffle if he has one", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owned
    });
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 1 //owning a snaffle
    });

    var ai = new AI(field);

    var moves = ai.moves();
    expect(moves.length).toBe(1);
    expect(moves[0].command()).toContain('THROW');
  });

  it("should order a wizard to move if there is an available snaffle", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owned
    });
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var ai = new AI(field);

    var moves = ai.moves();
    expect(moves.length).toBe(1);
    expect(moves[0].command()).toContain('MOVE 1000 2000');
  });

  it("should order a wizard to move to the available snaffle that is closer to our goal", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 3750,
      vx: 10,
      vy: 20,
      state: 0
    });
    field.addSnaffle({
      id: 2,
      x: 500,
      y: 3750,
      vx: 10,
      vy: 20,
      state: 0
    });
    field.addWizard({
      id: 1,
      x: 800,
      y: 3750,
      vx: 10,
      vy: 20,
      state: 0
    });

    var ai = new AI(field);

    var moves = ai.moves();
    expect(moves.length).toBe(1);
    expect(moves[0].command()).toContain('MOVE 500 3750');
  });

  it("should order a wizard to move and cover the goal if there are no snaffles", function() {
    var field = new Field(0);
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var ai = new AI(field);

    var moves = ai.moves();
    expect(moves.length).toBe(1);
    expect(moves[0].command()).toContain('MOVE');

    var goal = field.getOwnGoal();
    expect(moves[0].command()).toContain(goal.center.x + ' ' + goal.center.y);
  });

  it("should order a wizard to use PETRIFICUS when an enemy is going to score and we have no wizards to stop it", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 33,
      x: 100,
      y: 3750, //close to the left goal (our own goal)
      vx: 1, //with velocity
      vy: 1,
      state: 0
    });
    field.addEnemyWizard({
      id: 1,
      x: 500, //enemy close to score
      y: 3750,
      vx: 0,
      vy: 0,
      state: 0
    });
    field.addWizard({
      id: 1,
      x: 2000, //our wizard is too far
      y: 3750,
      vx: 0,
      vy: 0,
      state: 0
    });

    var ai = new AI(field);
    ai.setMagic(10); //10 or more required for PETRIFICUS

    field.calculateDistances();

    var moves = ai.moves();
    expect(moves.length).toBe(1);
    expect(moves[0].command()).toContain('PETRIFICUS 33');
  });

  it("should order a wizard to use ACCIO when the closest snaffle is even closer to an enemy", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 321,
      x: 10,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });
    field.addEnemyWizard({
      id: 1,
      x: 200,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });
    field.addWizard({
      id: 1,
      x: 400,
      y: 10,
      vx: 0,
      vy: 0,
      state: 0
    });

    var ai = new AI(field);
    ai.setMagic(15); //15 or more required for ACCIO

    field.calculateDistances();

    var moves = ai.moves();
    expect(moves.length).toBe(1);
    expect(moves[0].command()).toContain('ACCIO 321');
  });

  it("should order a wizard to move and cover the goal if another wizard is already going to catch the available snaffle", function() {
    var field = new Field(0);
    field.addSnaffle({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 0,
      vy: 0,
      state: 0 //not owned
    });
    field.addWizard({
      id: 1,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });
    field.addWizard({
      id: 2,
      x: 1000,
      y: 2000,
      vx: 10,
      vy: 20,
      state: 0 //not owning a snaffle
    });

    var ai = new AI(field);

    var moves = ai.moves();
    expect(moves.length).toBe(2);
    expect(moves[0].isCatch()).toBe(true);
    expect(moves[1].isCover()).toBe(true);
  });

});
