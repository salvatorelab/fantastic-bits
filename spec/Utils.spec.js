var Utils = require('../Utils.js');


describe("Utils", function() {
  it("should calculate the distance between a wizard and a snaffle", function() {
    var wizard = {
      x: 100,
      y: 100
    };

    var snaffle = {
      x: 0,
      y: 0
    };

    var distance = Utils.distanceFromWizardToSnaffle(wizard, snaffle);
    expect(distance).toBe(Math.sqrt(100*100 + 100*100));
  });
});
